

import Cocoa
import Foundation



//1
print("//1")

func factorial(_ number: Int) -> Int {
    if number == 1 {
        return 1
    }
    
    return number * factorial(number-1)
}

print(factorial(15))

//2
print("//2")

enum MethodSort {
    case bubbleSort
    case insertSort
}

func arraySort(array: [Int], method: MethodSort = MethodSort.bubbleSort) -> [Int] {
    var sorted: [Int]
    
    switch method {
    case .bubbleSort:
        sorted = bubbleSort(array: array)
    case .insertSort:
        sorted = bubbleSort(array: array)
    default:
        print("Eror!")
    }
    
    return sorted
}

func bubbleSort(array: [Int]) -> [Int] {
    var newArray: [Int] = array
    
    let length: Int = array.count
    var temp: Int = 0
    
    for i in 0..<length {
        for j in 1..<length-i {
            
            if newArray[j-1] > newArray[j] {
                temp = newArray[j-1]
                newArray[j-1] = newArray[j]
                newArray[j] = temp
            }
        }
    }
    
    return newArray
    
}

func insertionSort(array: [Int]) -> [Int] {
    var newArray: [Int] = array
    var j, newValue: Int
    
    let length: Int = array.count
    
    for i in 1..<length {
        
        newValue = newArray[i]
        j = i
        
        while j > 0 && newArray[j-1] > newValue {
            newArray[j] = newArray[j-1]
            j = j - 1
        }
        
        newArray[j] = newValue
    }
    
    
    
    return newArray
}



print(arraySort(array: [7,10,1,20,0,3,17,11,2,6], method: MethodSort.insertSort))

//3
print("//3")

extension String {
    var isEng: Bool {
        return range(of: "[^a-zA-Z0-9]", options: .regularExpression) == nil
    }
    
    var isUkr: Bool {
        return range(of: "[^а-яА-Я0-9]", options: .regularExpression) == nil
    }
}

func analyzeChar(character: String){
    var isLow: Bool = false
    var isLetter: Bool = false
    
    var (caseDesc, letterDesc, localeDesc) =  (" "," "," ")
    
    let lower = CharacterSet.lowercaseLetters
    let letters = CharacterSet.letters
    let digits = CharacterSet.decimalDigits
    
    
    if let scalar = UnicodeScalar(character) {
        isLow = lower.contains(scalar)
    }
    
    if let scalar = UnicodeScalar(character) {
        isLetter = letters.contains(scalar)
    }
    
    
    caseDesc = isLow ?  "LowerCase" : "UpperCase"
    letterDesc = isLetter ? "Letter" : "Digit"
    localeDesc = character.isEng ? "English" : "Ukrainian"
    
    
    print(localeDesc)
    print(caseDesc)
    print(letterDesc)
    
}

analyzeChar(character: "1")

//4
print("//4")

func analyzeString(_ str: String) {
    var chars: [Character] = [Character]()
    var containsNum = Array(repeating: 0, count: str.count)
    
    for symbol in str {
        var isContains: Bool = false
        
        
        for index in 0..<chars.count {
            if chars[index] == symbol {
                isContains = true
                containsNum[index] = containsNum[index] + 1
            }
        }
        
        if !isContains {
            chars.append(symbol)
        }
    }
    
    for index in 0..<chars.count {
        print("\(chars[index]) -- \(containsNum[index] + 1)")
    }
}

analyzeString("Serus Bratka!!!")


//6
print("//6")

typealias Circle = (Double, Double, Double)
typealias Point = (Double, Double)

let c1: Circle = (x: 1, y: 3, r: 5)
let c2: Circle = (x: -2, y: -3, r: 4)
let c3: Circle = (x: -3, y: 3, r: 3)
let c4: Circle = (x: 5, y: -1, r: 6)

let point: Point = (2, 2)

func checkPoint(_ circles: [Circle], _ point: Point) {
    
    var isInside: [Bool] = [Bool]()
    var largest: Circle = (0,0,0)
    
    for circle in circles {
        let x0 = circle.0
        let y0 = circle.1
        let r = circle.2
        
        let x = point.0
        let y = point.1
        
        let f = pow(x0 - x, 2) + pow(y0 - y, 2)
        
        if f <= pow(r,2) {
            isInside.append(true)
        } else {
            isInside.append(false)
        }
        
    }
    
    for i in 0..<isInside.count {
        if isInside[i] == true {
            
            if(circles[i].2 > largest.2){
                largest = circles[i]
            }
            
        }
    }
    
    print(isInside)
    print(largest)
}

checkPoint([c1,c2,c3,c4], point)



